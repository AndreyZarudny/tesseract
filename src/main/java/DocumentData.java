public class DocumentData {
    /** Идентификационный Код Закупки */
    private String purchaseIdentificationCode;
    /** Номер документа */
    private String docNumber;
    /** ИНН заказчика */
    private String customerInn;
    /** ИНН поставщика */
    private String supplierInn;
    /** Наименование заказчика */
    private String customerName;
    /** Наименование поставщика */
    private String supplierName;
    /** Дата заключения контракта */
    private String contractDate;
    /** Сумму контракта */
    private String contractSum;
    /** Пройден ли контроль */
    private boolean isControlPassage;

    public void setPurchaseIdentificationCode(String purchaseIdentificationCode) {
        this.purchaseIdentificationCode = purchaseIdentificationCode;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public void setCustomerInn(String customerInn) {
        this.customerInn = customerInn;
    }

    public void setSupplierInn(String supplierInn) {
        this.supplierInn = supplierInn;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public void setContractDate(String contractDate) {
        this.contractDate = contractDate;
    }

    public void setContractSum(String contractSum) {
        this.contractSum = contractSum;
    }

    public void setControlPassage(boolean controlPassage) {
        isControlPassage = controlPassage;
    }

    @Override
    public String toString() {
        return "DocumentData{" +
                "purchaseIdentificationCode='" + purchaseIdentificationCode + '\'' +
                ", docNumber='" + docNumber + '\'' +
                ", customerInn='" + customerInn + '\'' +
                ", supplierInn='" + supplierInn + '\'' +
                ", customerName='" + customerName + '\'' +
                ", supplierName='" + supplierName + '\'' +
                ", contractDate='" + contractDate + '\'' +
                ", contractSum='" + contractSum + '\'' +
                ", isControlPassage=" + isControlPassage +
                '}';
    }
}
