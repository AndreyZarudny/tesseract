import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;

public class Main {
    private static final String DATA_NOT_FOUND = "Данные %s не найдены";

//    private static final List<String> REQUIRED_PARAMS =
//            Arrays.asList("ИКЗ", //+
//                    "номер документа",
//                    "ИНН заказчика", //+
//                    "ИНН поставщика", //+
//                    "Наименование заказчика",
//                    "Поставщик",
//                    "Дата заключения контракта",
//                    "Сумму контракта");

    public static void main(String[] args) throws IOException {
//        ITesseract tesseract = new Tesseract();
//        tesseract.setDatapath("C:\\work\\Tesseract\\src\\main\\resources");
//        tesseract.setLanguage("rus");
//
//        File pdf = new File("C:\\work\\Tesseract\\src\\main\\resources\\Контракт_АКРА_290421-Р.pdf");
//        String text;
//
//        long start;
//        long result;
//
//        try {
//            start = System.currentTimeMillis();
//
//            text = tesseract.doOCR(pdf);
//
//            result = (System.currentTimeMillis() - start) / 1000;
//            System.out.println(result + " seconds");
//        } catch (TesseractException e) {
//            e.printStackTrace();
//        }

        //работал с текстом, который достал тессерактом
        String contentPDF;

        try(FileInputStream inputStream = new FileInputStream("C:\\work\\Tesseract\\src\\main\\resources\\contract.txt")) {
            contentPDF = IOUtils.toString(inputStream);
        }

        DocumentData documentData = new DocumentData();

        documentData.setPurchaseIdentificationCode(getPurchaseIdentificationCode(contentPDF));

        documentData.setCustomerInn(getCustomerInn(contentPDF));
        documentData.setSupplierInn(getSupplierInn(contentPDF));

//        boolean isControlPassage = checkPassageOfControl(contentPDF);
//        System.out.println(isControlPassage);
//        String s = getSupplierName(contentPDF);
//        System.out.println(s);

        System.out.println(documentData);
    }

    private static String getPurchaseIdentificationCode(String contentPDF) {
        String from = "(идентификационный код закупки";
        String to = ") о нижеследующем";

        if (StringUtils.containsIgnoreCase(contentPDF, from) && StringUtils.containsIgnoreCase(contentPDF, to)) {
            return StringUtils.substringBetween(contentPDF, from, to).replaceAll("\\D", "");
        }
        return String.format(DATA_NOT_FOUND, "об идентификационном коде закупки");
    }

    private static String getSupplierInn(String contentPDF) {
        String from = "инн";
        String to = "кпп";

        String detailsOfParties = getAddressesDetailsOfParties(contentPDF);
        String temp = StringUtils.substringBefore(detailsOfParties, to.toUpperCase());
        return StringUtils.substringAfter(temp, from.toUpperCase()).trim();
    }

    private static String getCustomerInn(String contentPDF) {
        String from = "инн";
        String to = "кпп";

        String detailsOfParties = getAddressesDetailsOfParties(contentPDF);
        String temp = StringUtils.substringAfter(detailsOfParties, to.toUpperCase());
        return StringUtils.substringBetween(temp, from.toUpperCase(), to.toUpperCase()).trim();
    }

//    private static String getSupplierName(String contentPDF) {
//        String to = "инн";
//        String from = "Исполнитель:";
//        String detailsOfParties = getAddressesDetailsOfParties(contentPDF);
//
//        String temp = StringUtils.substringBeforeLast(detailsOfParties, to.toUpperCase());
//        temp = StringUtils.substringAfter(temp, from);
//        String[] array = temp.split("\r\n");
//        return temp;
//    }

    private static String getAddressesDetailsOfParties(String contentPDF) {
        String from = "адреса и реквизиты сторон";
        String to = "техническое задание";

        if (StringUtils.containsIgnoreCase(contentPDF, from) && StringUtils.containsIgnoreCase(contentPDF, to)) {
            return StringUtils.substringBetween(contentPDF, from.toUpperCase(), to.toUpperCase());
        }
        return String.format(DATA_NOT_FOUND, "об адресах и реквизитах сторон");
    }

//    private static String checkPassageOfControl(String contentPDF) {
//        String numbers = contentPDF.replaceAll("\\D", " ");
//        List<String> array = Arrays.stream(numbers.split(" ")).filter(x -> x.length() > 5).collect(Collectors.toList());
//
//        for (String s : array) {
//            System.out.println(s);
//        }
//
//        return numbers; //todo обдумать эту тему (20 знаков имеется только в самом номере ИКЗ)
//    }
}
